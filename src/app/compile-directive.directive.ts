import { Directive, ViewContainerRef, Compiler, OnChanges, Input, ComponentRef, Component, NgModule, Type, ModuleWithComponentFactories, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[compile]'
})
export class CompileDirectiveDirective implements OnChanges {
  @Input()
  compile: string;
  @Input() compileContext: any;

  compRef: ComponentRef<any>;
  constructor(
    private vcRef: ViewContainerRef,
    private compiler: Compiler
  ) { }

  ngOnChanges() {
    if(!this.compile) {
      if(this.compRef) {
        this.updateProperties();
        return;
      }
      throw Error('You forgot to provide template');
    }

    console.log(this.compile)
    const component = this.createDynamicComponent(this.compile);
    const module = this.createDynamicModule(component);
    this.compiler.compileModuleAndAllComponentsAsync(module)
    .then(
      (moduleWidthComponentFactory: ModuleWithComponentFactories<any>) => {
        let compFactory = moduleWidthComponentFactory.componentFactories.find(x=> x.componentType === component);

        this.compRef = this.vcRef.createComponent(compFactory);
        this.updateProperties();
      }
    ).catch(error => {
      console.log(error);
    })
  }
  updateProperties() {
    // tslint:disable-next-line: forin
    for(let prop in this.compileContext) {
      this.compRef.instance[prop] = this.compileContext[prop];
    }
  }

  createDynamicComponent( template: string) {
    @Component({
      // tslint:disable-next-line: component-selector
      selector: 'custom-dynamic-component',
      template,
    })
    class CustomDynamicComponent {}
    return CustomDynamicComponent;
  }

  private createDynamicModule (component: Type<any>) {
    @NgModule({
      // You might need other modules, providers, etc...
      // Note that whatever components you want to be able
      // to render dynamically must be known to this module
      imports: [CommonModule],
      declarations: [component]

    })
    class DynamicModule {}
    return DynamicModule;
  }
}
