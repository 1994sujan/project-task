import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dynamic-project';
  templates = [
    {
        id: 1,
        template: 
        `<div class="main">
        <div class="card" style="width: 16rem;">
          <img src="{{data.image}}" class="card-img-top" alt="image.png">
          <div class="card-body">
            <div class="card-text">
              <p> Title: {{data.name}}  </p>
              <p> Description: {{data.brand}}</p>
              <p> Price: {{data.price}}</p>
            </div>
          </div>
        </div>
        </div>`
    }
  ];



  dynamicData = [
    {
    name: 'Iphone 7 Plus',
    brand: 'Iphone',
    image: '/assets/1p.jpg',
    price: '$1200'
  },
  {
    name: 'Samsung s10 Plus',
    brand: 'Samsung',
    image: '/assets/note.jpg',
    price: '$900'
  },
  {
    name: 'Iphone 8 Plus',
    brand: 'Iphone',
    image: '/assets/1p.jpg',
    price: '$1500'
  },
  {
    name: 'Samsung s9 Plus',
    brand: 'Samsung',
    image: '/assets/note.jpg',
    price: '$1000'
  },
  {
    name: 'Iphone 8 Plus',
    brand: 'Iphone',
    image: '/assets/1p.jpg',
    price: '$1500'
  },
  {
    name: 'Samsung s10 Plus',
    brand: 'Samsung',
    image: '/assets/note.jpg',
    price: '$900'
  },
  {
    name: 'Iphone 7 Plus',
    brand: 'Iphone',
    image: '/assets/1p.jpg',
    price: '$1500'
  },
  {
    name: 'Samsung s9 Plus',
    brand: 'Samsung',
    image: '/assets/note.jpg',
    price: '$1000'
  }
 ];
}
